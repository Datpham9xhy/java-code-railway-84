import java.util.Date;

public class Program {
    public static void main(String[] args) {
        Department dep1 = new Department();
        dep1.departmentID=1;
        dep1.departmentName="Marketing";
        System.out.println("Department ID: "+ dep1.departmentID+"\nDepartment Name: "+dep1.departmentName);


        Department dep2 = new Department();
        dep2.departmentID=2;
        dep2.departmentName="Sale";

        Department dep3 = new Department();
        dep3.departmentID=3;
        dep3.departmentName="Bảo vệ";

        Department dep4 = new Department();
        dep4.departmentID=4;
        dep4.departmentName="Nhân sự";

        Department dep5 = new Department();
        dep5.departmentID=5;
        dep5.departmentName="Kỹ thuật";

        Position position1 = new Position();
        position1.positionID=1;
        position1.positionName=PositionName.DEV;// ten enum.gtri
        System.out.println("positionID:"+position1.positionID+"\\n"+"positionName:"+position1.positionName);

        Position position2 = new Position();
        position2.positionID=2;
        position2.positionName=PositionName.TEST;

        Position position3 = new Position();
        position3.positionID=3;
        position3.positionName=PositionName.SCRUM_MASTER;

        Position position4 = new Position();
        position4.positionID=4;
        position4.positionName=PositionName.PM;

        Account account1 = new Account();
        account1.accountID=1;
        account1.email="nguyenthanhxuan@gmail.com";
        account1.userName="xuan";
        account1.fullName="nguyenthanhxuan";
        account1.departmentID=dep1;
        account1.positionID=position1;
        account1.createDate=new Date(2023-12-24);
        System.out.println("accountID: "+account1.accountID+"\n"+"email: "+account1.email+"\n"+"userName: "+ account1.userName+"\n"+
               "fullName: "+account1.fullName+"\n"+"departmentID: "+account1.departmentID+"\n"+"positionID: "+ account1.positionID+
                "\n"+"createDate: "+account1.createDate);

        Account account2 = new Account();
        account2.accountID=2;
        account2.email="nguyenvantruong@gmail.com";
        account2.userName="truong";
        account2.fullName="nguyenvantruong";
        account2.departmentID=dep2;
        account2.positionID=position2;
        account2.createDate=new Date(2023-2-24); // "2023-2-24": string

        Account account3 = new Account();
        account3.accountID=3;
        account3.email="lephuonganh@gmail.com";
        account3.userName="anh";
        account3.fullName="lephuonganh";
        account3.departmentID=dep3;
        account3.positionID=position3;
        account3.createDate=new Date(2022-3-20);

        Account account4 = new Account();
        account4.accountID=4;
        account4.email="dangmytam@gmail.com";
        account4.userName="tam";
        account4.fullName="dangmytam";
        account4.departmentID=dep4;
        account4.positionID=position4;
        account4.createDate=new Date(2019-5-22);

        Account account5 = new Account();
        account5.accountID=5;
        account5.email="maixuandieu@gmail.com";
        account5.userName="dieu";
        account5.fullName="maixuandieu";
        account5.departmentID=dep5;
        account5.positionID=position3;
        account5.createDate=new Date(2020-11-4);

        Group group1 = new Group();
        group1.groupID=1;
        group1.groupName="Testing System";
        group1.creatorID=account1;
        group1.createDate=new Date(2021-11-4);

        Group group2 = new Group();
        group2.groupID=2;
        group2.groupName="Management";
        group2.creatorID=account2;
        group2.createDate=new Date(2022-1-4);

        Group group3 = new Group();
        group3.groupID=3;
        group3.groupName="Development";
        group3.creatorID=account3;
        group3.createDate=new Date(2023-6-14);

        Group group4 = new Group();
        group4.groupID=4;
        group4.groupName="Marketing";
        group4.creatorID=account4;
        group4.createDate=new Date(2017-9-14);

        Group group5 = new Group();
        group5.groupID=5;
        group5.groupName="Creator";
        group5.creatorID=account4;
        group5.createDate=new Date(2019-9-18);

        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeID=1;
        typeQuestion1.typeName=TypeName.ESSAY;

        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeID=2;
        typeQuestion2.typeName=TypeName.MULTIPLE_CHOICE;

        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryID=1;
        categoryQuestion1.categoryName="Java";

        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryID=2;
        categoryQuestion2.categoryName="Python";

        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryID=3;
        categoryQuestion3.categoryName="C Sharp";

        CategoryQuestion categoryQuestion4 = new CategoryQuestion();
        categoryQuestion4.categoryID=4;
        categoryQuestion4.categoryName="PHP";

        CategoryQuestion categoryQuestion5 = new CategoryQuestion();
        categoryQuestion5.categoryID=5;
        categoryQuestion5.categoryName="Ruby";

        Question question1 = new Question();
        question1.questionID=1;
        question1.content="Java là gì";
        question1.categoryID=categoryQuestion1;
        question1.typeID=typeQuestion1;
        question1.creatorID=account1;
        question1.CreateDate=new Date(2019-9-18);

        Question question2 = new Question();
        question2.questionID=2;
        question2.content="Datatype là gì";
        question2.categoryID=categoryQuestion2;
        question2.typeID=typeQuestion2;
        question2.creatorID=account2;
        question2.CreateDate=new Date(2018-9-18);

        Question question3 = new Question();
        question3.questionID=3;
        question3.content="Backend là gì";
        question3.categoryID=categoryQuestion3;
        question3.typeID=typeQuestion2;
        question3.creatorID=account3;
        question3.CreateDate=new Date(2018-10-18);

        Question question4 = new Question();
        question4.questionID=4;
        question4.content="Có những ngôn ngữ nào viết backend";
        question4.categoryID=categoryQuestion4;
        question4.typeID=typeQuestion2;
        question4.creatorID=account4;
        question4.CreateDate=new Date(2020-1-8);

        Question question5 = new Question();
        question5.questionID=5;
        question5.content="long chứa bao nhiêu byte";
        question5.categoryID=categoryQuestion5;
        question5.typeID=typeQuestion1;
        question5.creatorID=account5;
        question5.CreateDate=new Date(2021-11-8);

        Answer answer1 = new Answer();
        answer1.answerID=1;
        answer1.content="answer1";
        answer1.questionID=question1;
        answer1.isCorrect=true;

        Answer answer2 = new Answer();
        answer2.answerID=2;
        answer2.content="answer2";
        answer2.questionID=question2;
        answer2.isCorrect=false;

        Answer answer3 = new Answer();
        answer3.answerID=3;
        answer3.content="answer3";
        answer3.questionID=question3;
        answer3.isCorrect=true;

        Answer answer4 = new Answer();
        answer4.answerID=4;
        answer4.content="answer4";
        answer4.questionID=question4;
        answer4.isCorrect=false;

        Answer answer5 = new Answer();
        answer5.answerID=5;
        answer5.content="answer5";
        answer5.questionID=question5;
        answer5.isCorrect=true;

        Exam exam1 = new Exam();
        exam1.examID=1;
        exam1.code="VTIQ001";
        exam1.title="Đề thi C#";
        exam1.categoryID=categoryQuestion1;
        exam1.creatorID=account1;
        exam1.createDate=new Date(2021-10-8);

        Exam exam2 = new Exam();
        exam2.examID=2;
        exam2.code="VTIQ002";
        exam2.title="Đề thi Java";
        exam2.categoryID=categoryQuestion1;
        exam2.creatorID=account2;
        exam2.createDate=new Date(2020-12-8);

        Exam exam3 = new Exam();
        exam3.examID=3;
        exam3.code="VTIQ003";
        exam3.title="Đề thi Python";
        exam3.categoryID=categoryQuestion3;
        exam3.creatorID=account3;
        exam3.createDate=new Date(2022-1-28);

        Exam exam4 = new Exam();
        exam4.examID=4;
        exam4.code="VTIQ004";
        exam4.title="Đề thi ADO.NET";
        exam4.categoryID=categoryQuestion4;
        exam4.creatorID=account5;
        exam4.createDate=new Date(2023-11-21);

        Exam exam5 = new Exam();
        exam5.examID=5;
        exam5.code="VTIQ005";
        exam5.title="Đề thi Postman";
        exam5.categoryID=categoryQuestion5;
        exam5.creatorID=account5;
        exam5.createDate= new Date(2017-10-15);
    }


}
