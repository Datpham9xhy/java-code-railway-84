import java.util.Date;

public class Exam {
    int examID;
    String code;
    String title;
    CategoryQuestion categoryID;
    int duration;
    Account creatorID;
    Date createDate;
    Question[] questions;
}
